#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <math.h>
#include <sstream>
#include <algorithm>

using namespace std;


int check_if_win(int row, int col, vector<vector<vector<int>>> boards){
	
	int count = 0;

	for (int b = 0; b < boards.size(); b++){
		cout << "board " << b << endl;
		for(int j = 0; j < col; j++){
			cout << "j = " << j << endl;
			if (boards[b][0][j] == -1){
				cout << "found smthing at  0" << j << " " << endl;
				count++;
				for (int i = 1; i < row; i++){
					if (boards[b][i][j] == -1){
						count++;
						cout << "found smthing at  " << i << j << " increasing count" << count << endl;
					} else {
						cout << "broke at  " << i << j <<  endl;
						//cin.get();
						count = 0;
						break;
					}
					if (count == 5){
						return b+1;
					}

				}
			}
		}
		for(int i = 0; i < row; i++){
			cout << "i = " << i << endl;
			if (boards[b][i][0] == -1){
				cout << "found smthing at  "<< i << "0" << " " << endl;
				count++;
				for (int j = 1; j < row; j++){
					if (boards[b][i][j] == -1){
						count++;
						cout << "found smthing at  " << i << j << " increasing count" << count << endl;
					} else {
						count = 0;
						cout << "broke at  " << i << j <<  endl;
						//cin.get();
						break;
					}
					if (count == 5){
						return b+1;
					}

				}
			}
		}
	
	}
	return 0;

}




int calculate_win(int row, int col, int winning_num, vector<vector<int>> winning_board){
	int score = 0;
	for (int i = 0; i < row; i++){
		for (int j = 0; j < col; j++){
			if (winning_board[i][j] != -1){
				score += winning_board[i][j];
			}
		}
	}
	score *= winning_num;
	return score;

}




int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	
	int output1 = 0;
	int output2 = 0;

	int i = 0, j = 0;

	int row = 5, col = 5, num_boards = 0;
	
	vector<vector<vector<int>>> vboards;
	vector<int> vboards_row;
	vector<int> vnum_draw;

	string num_draw_line;
	string board_digit;
	string token;
	finput >> num_draw_line;
	istringstream iss(num_draw_line);

	
	while (getline(iss,token,',')){
		vnum_draw.push_back(stoi(token));
	}

	for (int i = 0; i < vnum_draw.size(); i++){
		cout<<vnum_draw[i] << " ";
	}	
	cout << endl << endl;
	


	while (finput >> board_digit){
		
		cout << "Digit : " << endl << board_digit << " ";
		vboards_row.push_back(stoi(board_digit));

		for (i = 0; i < col - 1; i++){
			finput >> board_digit;
			cout << board_digit << " ";
			vboards_row.push_back(stoi(board_digit));
		}

		if (j%row == 0){
			num_boards++;
			vboards.push_back(vector<vector<int>>());
		}

		vboards[num_boards-1].push_back(vboards_row);
		j++;
		vboards_row = vector<int>();
		cout << endl;

	}

	int is_win = 0, turn = 0, cur_num;
	while (!is_win){
		cur_num = vnum_draw[turn];
		cout << "Current number : " << cur_num << endl << endl;

		for (int b = 0; b < vboards.size();b++){
			for(i = 0; i < col; i++){
				for(j = 0; j < col; j++){
					if (vboards[b][i][j] == cur_num){
						vboards[b][i][j] = -1;
					}
				}
			}
		}

		for (int b = 0; b < num_boards; b++){
			for (i = 0; i < row; i++){
				for (j = 0; j < col; j++){
					cout << vboards[b][i][j] << " | ";
				}
				cout<<endl;
			}
				cout<<endl;
		}
		//cin.get();
		is_win = check_if_win(row,col,vboards);
		turn++;

	}
	
	output1 = calculate_win(row,col,cur_num,vboards[is_win-1]);
	cout << "REMOVING BOARD " << is_win << endl;


	vboards.erase(vboards.begin()+is_win-1);
	num_boards--;


	while (vboards.size() != 1){
		cur_num = vnum_draw[turn];
		cout << "Current number : " << cur_num << endl << endl;

		for (int b = 0; b < vboards.size();b++){
			for(i = 0; i < col; i++){
				for(j = 0; j < col; j++){
					if (vboards[b][i][j] == cur_num){
						vboards[b][i][j] = -1;
					}
				}
			}
		}

		for (int b = 0; b < num_boards; b++){
			for (i = 0; i < row; i++){
				for (j = 0; j < col; j++){
					cout << vboards[b][i][j] << " | ";
				}
				cout<<endl;
			}
				cout<<endl;
		}
		
		is_win = check_if_win(row,col,vboards);

		cout << is_win << endl;
		//cin.get();

		if (is_win){
			while (is_win){
				vboards.erase(vboards.begin()+is_win-1);
				num_boards--;
				is_win = check_if_win(row,col,vboards);
			}
		}
		turn++;
	}
	is_win = 0;
	while (!is_win){
		cur_num = vnum_draw[turn];
		cout << "Current number : " << cur_num << endl << endl;
		
		for (int b = 0; b < vboards.size();b++){
			for(i = 0; i < col; i++){
				for(j = 0; j < col; j++){
					if (vboards[b][i][j] == cur_num){
						vboards[b][i][j] = -1;
					}
				}
			}
		}

		for (int b = 0; b < num_boards; b++){
			for (i = 0; i < row; i++){
				for (j = 0; j < col; j++){
					cout << vboards[b][i][j] << " | ";
				}
				cout<<endl;
			}
				cout<<endl;
		}
		
		is_win = check_if_win(row,col,vboards);
	}

	output2 = calculate_win(row,col,cur_num,vboards[0]);



	cout << "######################################" << endl << endl;


	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl;

	return 0;

}
