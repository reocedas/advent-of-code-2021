#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#include <cstring>
#include <sstream>
#include <map>

using namespace std;






int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	
	vector<int> input;
	vector<int> position;

	string sinput,token;

		
	int output1 = 0;
	int output2 = 0;			//output variables

	int fuel = 0;

	int pos_max =  numeric_limits<int>::min();	//
	int pos_min =  numeric_limits<int>::max();	// variables to know which range to consider. Probably I can shrink it.


	int min_fuel = numeric_limits<int>::max();

	finput >> sinput;
	istringstream iss(sinput);

	while (getline(iss,token,',')){			// get input from file and separate at coma
		cout << token << " ";		
		input.push_back(stoi(token));
		if (stoi(token) < pos_min){
			pos_min = stoi(token);
		}
		if (stoi(token) > pos_max){
			pos_max = stoi(token);
		}
	}

	for (int i = pos_min; i <= pos_max; i++){
		position.push_back(i);
	}						// generate the vector of possible positions
	
	
	for (int i = 0; i < position.size(); i++){	// PART 1 : for each possible position, just calculate the difference
							// between the current considered position and all the crab position
							// and add them up. Keep the value if lower than the previous value kept.
		fuel = 0;
		for (int j = 0; j < input.size(); j++){
			fuel += abs(position[i]-input[j]);
		}
		cout << "position # " << position[i] << " " << fuel << endl;
		if (min_fuel > fuel){
			min_fuel = fuel;
		}
	}


	output1 = min_fuel;
	min_fuel = numeric_limits<int>::max();


	for (int i = 0; i < position.size(); i++){	// PART 2 : for each crab position, calculate the arithmetic
							// sum from 0 to the distance value
							// keep the value if lower than the previsou value kept.
		fuel = 0;
		for (int j = 0; j < input.size(); j++){
			fuel += abs(position[i]-input[j])*(abs(position[i]-input[j])+1)/2;
		}
		cout << "position # " << position[i] << " " << fuel << endl;
		if (min_fuel > fuel){
			min_fuel = fuel;
		}
	}
	output2 = min_fuel;
	

	
	
	cout << endl << "######################################" << endl << endl;


	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl << endl;

	cout << "######################################" << endl << endl;
	return 0;

}
