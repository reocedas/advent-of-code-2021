#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#include <cstring>
#include <sstream>
#include <map>

using namespace std;



vector<int> do_cycle(vector<int> fishes){		// function that automatically goes through the fishes and see
							// hich ones will reproduce
	int add_fishes = 0;
	for (int i = 0; i < fishes.size(); i++){
		if (fishes[i] == 0){
			add_fishes++;
			fishes[i]=6;
		} else {
			fishes[i]--;
		}
	}
	for (int i = 0; i < add_fishes; i++){
		fishes.push_back(8);
	}
	return fishes;
}


map<int, long long> do_cycle_2(map<int, long long> fishes){
	
	map<int, long long> fishes_buf;

	for (int i = 1; i <= 8; i++){
		fishes_buf[i-1]=fishes[i];
	}

	fishes_buf[8] += fishes[0];
	fishes_buf[6] += fishes[0];


	return fishes_buf;
}


map<int, long long>  re_input(vector<int> input){
	
	map<int, long long> new_input;
	int count = 0;

	for (int i = 0; i <= 8; i++){
		count = 0;
		for (int j = 0; j < input.size(); j++){
			if (input[j] == i){
				count ++;
			}
		}
		new_input[i] = count;
	}
	return new_input;
}




int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	
	vector<int> input;
	map<int, long long> var_input; 		// <----- this one's necessary, because for part 2 
						//        we need to change the way we count
	string sinput,token;

		
	int output1 = 0;
	long long output2 = 0;			//output variables

	
	finput >> sinput;
	istringstream iss(sinput);

	while (getline(iss,token,',')){
		cout << token << " ";		
		input.push_back(stoi(token));
	}
	
	
	var_input = re_input(input);
	for (int i = 0; i < 80; i++){
		cout << "cycle # " << i << " : " << input.size() << " fishes." << endl;
		input = do_cycle(input);
	}
	output1 = input.size();
	
	//var_input = re_input(input);


	
		for (int j = 0; j <= 8; j++){
			cout << j << " -> " << var_input[j] << " ";
		}
		cout << endl;
	for (int i = 0; i < 256; i++){
		cout << "cycle # " << i << endl;
		var_input = do_cycle_2(var_input);
		for (int j = 0; j <= 8; j++){
			cout << j << " -> " << var_input[j] << " ";
		}
		//cin.get();
	}


	cout << output2 << endl;
	for (int i  = 0; i < var_input.size(); i++){
		cout << i << " : " << var_input[i] << endl;
		output2 += var_input[i];
	}
	
	
	cout << "######################################" << endl << endl;


	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl << endl;

	cout << "######################################" << endl << endl;
	return 0;

}
