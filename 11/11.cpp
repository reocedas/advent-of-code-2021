#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#include <cstring>
#include <sstream>
#include <map>
#include <algorithm>

using namespace std;



int **update_octopus(int **octopus){
	int i, j;
	bool state_change = false;
	int count = 0;


	int **octopus_state;				// state of the octopuses initialized to 0
	octopus_state = new int *[10];
	for (i = 0; i < 10; i++){
		octopus_state[i] = new int[10];
	}

	for (i = 0; i < 10 ; i++){			// increment all cells
		for (j = 0; j < 10; j++){
			octopus[i][j]++;
		}
	}

	do{	
		state_change = false;
		for (i = 0; i < 10 ; i++){
			for (j = 0; j < 10; j++){
				if (octopus[i][j] > 9 and octopus_state [i][j] != 1){ 		// if the state of the cell makes it flash and that it didn't already flash
					octopus_state[i][j] = 1;
					if (i != 9){
						octopus[i+1][j]++;
					}
					if (i != 0){
						octopus[i-1][j]++;
					}
					if (j != 9){
						octopus[i][j+1]++;
					}
					if (j != 0){
						octopus[i][j-1]++;
					}
					if (i != 9 and j != 9){
						octopus[i+1][j+1]++;
					}
					if (i != 0 and j != 0){
						octopus[i-1][j-1]++;
					}
					if (j != 9 and i != 0){
						octopus[i-1][j+1]++;
					}
					if (j != 0 and i != 9){
						octopus[i+1][j-1]++;
					}

					state_change = true;
				}
			}
		}
		count++;

	} while (state_change);				// do it while there are new cells that flash


	for (i = 0; i < 10 ; i++){			// cells that did flash go back to 0
		for (j = 0; j < 10; j++){
			if (octopus[i][j] > 9){
				octopus[i][j] = 0;
			}
		}
	}						

	return octopus;					// returns the new state
}


int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	string sinput,num;
	
	

	int i, j, k;
	
	bool non_zero;

	int **octopus;				// state of the octopuses initialized to 0
	octopus = new int *[10];
	for (i = 0; i < 10; i++){
		octopus[i] = new int[10];
	}

	int output1 = 0;
	int output2 = 0;			//output variables

	i=0;
	while (finput >> sinput){		//read the input
		for (j = 0; j < 10; j++){
			num = sinput[j];
			octopus[i][j] = stoi(num);
		}
		i++;
	}

	for (i = 0; i < 10 ; i++){		// print initial state
		for (j = 0; j < 10; j++){
			cout << octopus[i][j] << " ";
		}
			cout << endl;
	}	

	for (k = 0; k < 100; k++){                		// PART 1
		non_zero = false;                 		//
		cout << k << endl << endl;        		// caclulates the new state for each new cycle
		octopus = update_octopus(octopus);		// and counts the number of 0s in the array after
		for (i = 0; i < 10 ; i++){			// updating. That's the number of flashes, and it
			for (j = 0; j < 10; j++){		// adds up to output1.
				if (octopus[i][j] == 0){     	//
					output1++;           	//
				} else {                     	//
					non_zero = true;     	//
				}                            	//
			}                                    	//
		}                                            	//
	}							// PART 1
	
	while (output2 == 0){					// PART 2
		non_zero = false;                       	//
		cout << k << endl << endl;              	// do the same as part 1, but continues while output2 ==0
		octopus = update_octopus(octopus);      	// if a value not equal to 0 is detected in the updated array
		for (i = 0; i < 10 ; i++){              	// output2 doesn't change. If it does, it takes k+1 value (index of the cycle)
			for (j = 0; j < 10; j++){       	//
				if (octopus[i][j] == 0){	//
					output1++;      	//
				} else {                	//
					non_zero = true;	//
				}                       	//
			}                               	//
		}                                       	//
                                                        	//
		if (!non_zero){                         	//
			output2 = k+1;                  	//
		}                                       	//
		k++;                                    	//
                                                        	//
	}                                               	// PART 2


	cout << endl << "######################################" << endl << endl;


	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl << endl;

	cout << "######################################" << endl << endl;
	return 0;

}
