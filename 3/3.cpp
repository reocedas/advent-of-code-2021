#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>

using namespace std;


int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	
	int output1 = 0;
	int output2 = 0;
	
	vector<string> vinput;
	vector<string> vinput_o2;
	vector<string> vinput_co2;
	vector<string> vinput_o2_buf;
	vector<string> vinput_co2_buf;

	string input;
	
	string gamma;
	int gamma_dec;
	int eps = 0;

	int dat_len;
	int num_input=0;
	int num_input_o2=0;
	int num_input_co2=0;

	int remove_val;
	
	int value = 0;
	char cur_in;


	while (finput >> input){
		vinput.push_back(input);
		num_input++;
	}
	vinput_o2 = vinput;
	vinput_co2 = vinput;
	
	dat_len = input.length();

	for (int i = 0; i < dat_len; i++){

		value = 0;
		for (int j = 0; j < vinput.size(); j++){
			cur_in = vinput[j][i];
			if (cur_in == '1'){
				value += 1;
			}
		}
		


		if (value > num_input/(double)2){
			gamma += "1";
			//eps += "0";
		} else {
			gamma += "0";
			//eps += "1";
		}



		
		value = 0;
		num_input_o2 = 0;	
		if (vinput_o2.size() > 1){
			for (int j = 0; j < vinput_o2.size(); j++){
				num_input_o2++;
				cur_in = vinput_o2[j][i];
				if (cur_in == '1'){
					value += 1;
				}
			}

			cout << "Num o2 " << num_input_o2 << endl;

			if (value >= num_input_o2/(double)2){
				remove_val = '0';
				
				cout << "Want to remove 0" << endl;
			} else {
				remove_val = '1';
				cout << "Want to remove 1" << endl;
			}
			int o2_size = vinput_o2.size();
			cout << "Removing.";
			for (int j = 0; j < o2_size; j++){
				cur_in = vinput_o2[j][i];
				if (cur_in == remove_val){
					cout << "." ;
				} else {
					vinput_o2_buf.push_back(vinput_o2[j]);
				}
		}

		vinput_o2 = vinput_o2_buf;
		vinput_o2_buf.clear();
		cout << endl << endl;
	}
		value = 0;
		num_input_co2 = 0;	
		if (vinput_co2.size() > 1){
			for (int j = 0; j < vinput_co2.size(); j++){
				num_input_co2++;
				cur_in = vinput_co2[j][i];
				if (cur_in == '1'){
					value += 1;
				}
			}
			cout << "Num o2 " << num_input_o2 << endl;
			if (value >= num_input_co2/(double)2){
				remove_val = '1';
			} else {
				remove_val = '0';
			}
			int co2_size = vinput_co2.size();
			cout << "Removing.";
			for (int j = 0; j < co2_size; j++){
				cur_in = vinput_co2[j][i];

				if (cur_in == remove_val){
					cout << "." ;
				} else {
					vinput_co2_buf.push_back(vinput_co2[j]);
				}
		}

		vinput_co2 = vinput_co2_buf;
		vinput_co2_buf.clear();
		cout << endl << endl;
	}
	}

	cout << "######################################" << endl << endl;

	eps = (int)(pow(2,dat_len)+0.5) -1;

	cout << gamma << " " << stoi(gamma,0,2) << endl;
	gamma_dec = stoi(gamma,0,2);
	
	eps = gamma_dec ^ eps;

	output1 = eps * gamma_dec;

	output2 = stoi(vinput_o2[0],0,2)*stoi(vinput_co2[0],0,2);

	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl;

	return 0;

}
