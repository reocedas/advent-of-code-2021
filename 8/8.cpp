#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#include <cstring>
#include <sstream>
#include <map>

using namespace std;

int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	
	string PH;	
	
	vector<vector<string>> input;
	vector<string> input_buf{PH,PH,PH,PH,PH,
			     PH,PH,PH,PH,PH};
	
	vector<vector<string>> numbers;
	vector<string> numbers_buf{PH,PH,PH,PH};


	string sinput,token;

	map<int, string> mapping;

	for (int i = 0; i < 8; i++){
		mapping[i] = PH;
	}

	int count = 0;	

	int output1 = 0;
	int output2 = 0;			//output variables

	map<string, string> my_numbers;
	map<int, string> numbers_my;
	

	string number_1;
	string number_2;

	while (finput >> input_buf[0] >> input_buf[1] >> input_buf[2] >> input_buf[3] >> input_buf[4]
		      >> input_buf[5] >> input_buf[6] >> input_buf[7] >> input_buf[8] >> input_buf[9]
		      >> PH >> numbers_buf[0] >> numbers_buf[1] >> numbers_buf[2] >> numbers_buf[3]){	//read the input line

		input.push_back(input_buf);								// add the input line to the main input stack
		numbers.push_back(numbers_buf);								// add the input digit to decypher to the main stack

	}


	////////////////////////////////////////////////////////////// PART 1
	
	for (int i = 0; i < numbers.size(); i++){
		for (int j = 0; j < numbers[i].size(); j++){
		       	if (numbers[i][j].size() == 2 or
			    numbers[i][j].size() == 3 or
			    numbers[i][j].size() == 4 or
			    numbers[i][j].size() == 7 ){

				count ++;
			}
	
		}
	}

													// count the number of str in numers that have the right size
	output1 = count;
	
	////////////////////////////////////////////////////////////// PART 1

	////////////////////////////////////////////////////////////// PART 2

	string buffer;
	vector<int> end;
	vector<string> num_to_compare;
	

	for (int i = 0; i < input.size(); i++){
		my_numbers.clear();
		numbers_my.clear();
		buffer.clear();
		num_to_compare.clear();
		mapping.clear();


		for (int j = 0; j < input[i].size(); j++){			// find 1 and 7 and store there expr
		       	if (input[i][j].size() == 2){
				number_1 = input[i][j];
				my_numbers[input[i][j]] = '1';
				numbers_my[1] = input[i][j];
			} else if (input[i][j].size() == 3){
				number_2 = input[i][j];
				my_numbers[input[i][j]] = '7';
				numbers_my[7] = input[i][j];
			}
		}


	
		for (int j = 0; j < number_2.size();j++){			// compare 1 and 7 to find segment a
			
			if (number_1.find(number_2[j]) == string::npos){
				mapping[0] = number_2[j];
			}
		}
	




		for (int j = 0; j < input[i].size(); j++){			// find 1 and 4 and store 4 expr
		       	if (input[i][j].size() == 2){
				number_1 = input[i][j];
			} else if (input[i][j].size() == 4){
				number_2 = input[i][j];
				my_numbers[input[i][j]] = '4';
				numbers_my[4] = input[i][j];
			}
		}



		for (int j = 0; j < number_2.size();j++){			// compare 1 and 4 to find segment b and d
			
			if (number_1.find(number_2[j]) == string::npos){
				buffer += number_2[j];
			}

		}

		
		for (int j = 0; j < input[i].size(); j++){			// find 2, 3 and 5
		       	if (input[i][j].size() == 5){
				num_to_compare.push_back(input[i][j]);
			}
		}
		


		for (int j = 0; j <num_to_compare.size(); j++){			// finds segment b, then d by deduction
			for (int k = 0; k < buffer.size(); k++){
				if (num_to_compare[j].find(buffer[k]) == string::npos){
					
					mapping[1] = buffer[k];
					
					buffer.erase(buffer.begin()+k);
					
					mapping[3] = buffer[0];
					break;
				}
			}
		}

		for (int j = 0; j <num_to_compare.size(); j++){			// find and store 5 expr
			if (num_to_compare[j].find(mapping[1]) != string::npos){
				my_numbers[num_to_compare[j]] = '5';
				numbers_my[5] = num_to_compare[j];
			}
		}
		
		buffer.clear();
		num_to_compare.clear();


		//////////////////////
		buffer = numbers_my[4];

		
		buffer.erase(buffer.begin() + buffer.find(mapping[1]));
		buffer.erase(buffer.begin() + buffer.find(mapping[3]));		

		//////////////////////						// change buffer stat to prepare 2 and 3 finding

		

		for (int j = 0; j < input[i].size(); j++){			// find 2, 3
		       	if (input[i][j].size() == 5 and input[i][j] != numbers_my[5]){
				num_to_compare.push_back(input[i][j]);
			}
		}

		
		for (int j = 0; j <num_to_compare.size(); j++){			// finds segment f, then c by deduction
			for (int k = 0; k < buffer.size(); k++){
				if (num_to_compare[j].find(buffer[k]) == string::npos){
					mapping[5] = buffer[k];
					buffer.erase(buffer.begin()+k);
					mapping[2] = buffer[0];
					break;
				}
			}
		}
		
		
		for (int j = 0; j <num_to_compare.size(); j++){			// find and store 3 expr
			if (num_to_compare[j].find(mapping[5]) != string::npos){
				my_numbers[num_to_compare[j]] = '3';
				numbers_my[3] = num_to_compare[j];
			}
		}
		
		for (int j = 0; j < input[i].size(); j++){			// find and store 2 expr
		       	if (input[i][j].size() == 5 and input[i][j] != numbers_my[5] and input[i][j] != numbers_my[3]){
				my_numbers[input[i][j]] = '2';
				numbers_my[2] = input[i][j];
				
			}
		}
		
		
		for (int j = 0; j < input[i].size(); j++){			// find and store 0 , 6 and 9 expr
		       	if (input[i][j].size() == 6){
				if (input[i][j].find(mapping[3]) == string::npos){
					my_numbers[input[i][j]] = '0';
					numbers_my[0] = input[i][j];
				} else if (input[i][j].find(mapping[2]) == string::npos){
					my_numbers[input[i][j]] = '6';
					numbers_my[6] = input[i][j];
				} else {
					my_numbers[input[i][j]] = '9';
					numbers_my[9] = input[i][j];
				}

				
			}
		}
		
		for (int j = 0; j < input[i].size(); j++){			// find and store 8 expr
		       	if (input[i][j].size() == 7){
				my_numbers[input[i][j]] = '8';
				numbers_my[8] = input[i][j];
			}
		}

		
		buffer.clear();
		
		
		for (int k = 0; k < numbers[i].size(); k++){			// decypher the 4 digit numbers
			for (int j = 0; j < input[i].size(); j++){
				if (input[i][j].size() == numbers[i][k].size()){
					for (int l = 0; l < input[i][j].size(); l++){
						if (numbers[i][k].find(input[i][j][l]) == string::npos){
							break;
						} else if (l == input[i][j].size()-1) {
							buffer += my_numbers[input[i][j]];
						}
					}
				}
			}
		}

		end.push_back(stoi(buffer));
	}

	for (int i = 0; i < end.size(); i++){
		output2 += end[i];
	}


	cout << endl << "######################################" << endl << endl;


	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl << endl;

	cout << "######################################" << endl << endl;
	return 0;

}
