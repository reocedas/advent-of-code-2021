#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>

using namespace std;




int **update_diagram(int min_x, int min_y, vector<int> coord, int **diagram){	/* function that updates the array automatically 
										and returns the updated state */
	
	int x1 = coord[0], y1 = coord[1], x2 = coord[2], y2 = coord[3];
	int i = 0;


	if (x1 == x2){
		for (i = min(y1,y2); i <= max(y1,y2); i++){
			diagram[x1-min_x][i-min_y]++;
		}
	} else if (y1 == y2){
		for (i = min(x1,x2); i <= max(x1,x2); i++){
			diagram[i-min_x][y1-min_y]++;
		}
	}
	return diagram;
}

int **update_diagram_2(int min_x, int min_y, vector<int> coord, int **diagram){	/* function that updates the array automatically 
										and returns the updated state for PART2 */
	
	int x1 = coord[0], y1 = coord[1], x2 = coord[2], y2 = coord[3];
	int x=x1, y = y1; 				// variable for current position
	int i = 1;


	if ((int)(abs(x1- x2)) == (int)(abs(y1- y2))){
		while (x != x2){
			diagram[x-min_x][y-min_y]++;
			x = x + ((x2-x1)/abs(x2-x1))*i;
			y = y + ((y2-y1)/abs(y2-y1))*i;
		}	

		diagram[x-min_x][y-min_y]++;
	}
	return diagram;
}


int count_overlap(int min_x, int min_y, int max_x, int max_y, int **diagram){ // function that counts the number of cells >1
	
	int count=0;

	for (int i = 0; i < (max_x-min_x+1); i++){
		for (int j = 0; j < (max_y-min_y+1); j++){
			if (diagram[j][i] > 1){
				count++;
			}	
		}
	}
	return count;
}



int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	
	int output1 = 0;
	int output2 = 0;			//output variables

	vector<vector<int>> coordinates;	// stores the coordinates line by line
	vector<int> coord_buf;			// buffer for the coordinates per line

	int x1,x2,y1,y2; 			// variables to gather coordinates
	
	int max_x = numeric_limits<int>::min(),
	    max_y = numeric_limits<int>::min(),
	    min_x = numeric_limits<int>::max(),
	    min_y = numeric_limits<int>::max(); // max and min variables


	auto delim = char{'\0'};		// placeholder for delimiters


	
	while (finput >> x1 >> delim >> y1 >> delim >> delim >> x2 >> delim >> y2){ 	//read each line
		

		if (x1 > max_x){
			max_x = x1;
		}
		if (x2 > max_x){
			max_x = x2;
		}
		if (y1 > max_y){
			max_y = y1;
		}
		if (y2 > max_y){
			max_y = y2;
		}


		if (x1 < min_x){
		        min_x = x1;
		}
		if (x2 < min_x){
		        min_x = x2;
		}
		if (y1 < min_y){
		        min_y = y1;
		}
		if (y2 < min_y){
			min_y = y2;
		}				// update the min max values for the array size
		coord_buf.push_back(x1);
		coord_buf.push_back(y1);
		coord_buf.push_back(x2);
		coord_buf.push_back(y2);	// place the gathered coordinates inside the buffer
		
		coordinates.push_back(coord_buf);
		coord_buf.clear();		// push the buffer inside the actual coordinate storage and reset the buffer

	}


	int **diagram;				// state diagram initialized to 0
	diagram = new int *[max_x-min_x+1];
	for (int i = 0; i < (max_x-min_x+1); i++){
		diagram[i] = new int[max_y-min_y+1];
	}


	for (int c = 0; c < coordinates.size(); c++){	// update diagram for x1=x2 or y1=y2 		(Part1)
		if (coordinates[c][0]==coordinates[c][2] or coordinates[c][1]==coordinates[c][3]){
			diagram=update_diagram(min_x, min_y, coordinates[c], diagram);
		}
	}


	output1 = count_overlap(min_x,min_y,max_x,max_y,diagram);	// calculating output1

	for (int c = 0; c < coordinates.size(); c++){	// update diagram for x1=x2 or y1=y2 		(Part1)
		diagram=update_diagram_2(min_x, min_y, coordinates[c], diagram);
		
		
		/*for (int i = 0; i < (max_x-min_x+1); i++){					// printing diagram (for debug)
			for (int j = 0; j < (max_y-min_y+1); j++){
				cout << diagram[j][i] << " ";
			}
			cout << endl;
	}
			//cin.get();*/

	}


	output2 = count_overlap(min_x,min_y,max_x,max_y,diagram);	// calculating output1
	
	cout << "######################################" << endl << endl;


	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl << endl;

	cout << "######################################" << endl << endl;
	return 0;

}
