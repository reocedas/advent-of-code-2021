#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#include <cstring>
#include <sstream>
#include <map>
#include <algorithm>

using namespace std;

int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	string sinput, check_chunk;

	int len=0;
	
	int output1 = 0;
	long long output2 = 0;			//output variables

	bool corrupted;
	vector<long long> scores;


	while (finput >> sinput){		//read the input line
		check_chunk.clear();
		corrupted = false;
		for (int i = 0; i < sinput.size(); i++){
			if (sinput[i] == '[' or						//	PART 1
			    sinput[i] == '(' or						//
			    sinput[i] == '{' or						//
			    sinput[i] == '<'){						// checks if the current character is 
				check_chunk += sinput[i];				// an opening character. If yes, just
			} else if (sinput[i] == ')'){					// adds it to a storing string.
				if (check_chunk[check_chunk.size()-1] != '('){  	//
					output1 += 3;                           	// If not, then checks if the closing character
					corrupted = true;				// matches the last character added to the storing.
					break;                                  	//
				} else {                                        	// If it does, removes the last char in the storing
					check_chunk.erase(check_chunk.end()-1);		// and continues. If it doesn't, adds the corresponding
				}	                                        	// score to output1 and move on to the next string.
			} else if (sinput[i] == ']'){                           	//
				if (check_chunk[check_chunk.size()-1] != '['){  	//
					output1 += 57;                          	//
					corrupted = true;				//
					break;                                  	//
				} else {                                        	//
					check_chunk.erase(check_chunk.end()-1);		//
				}                                               	//
			} else if (sinput[i] == '}'){                           	//
				if (check_chunk[check_chunk.size()-1] != '{'){  	//
					output1 += 1197;                        	//
					corrupted = true;				//
					break;                                  	//
				} else {                                        	//
					check_chunk.erase(check_chunk.end()-1);		//
				}                                               	//
			} else if (sinput[i] == '>'){                           	//
				if (check_chunk[check_chunk.size()-1] != '<'){  	//
					output1 += 25137;                       	//
					corrupted = true;				//
					break;                                  	//
				} else {                                        	//
					check_chunk.erase(check_chunk.end()-1);		//	PART 1
				}							//
			}								//

		}

		if (!corrupted){                                                 	//	PART 2
			scores.push_back(0);                                     	//
			while (!check_chunk.empty()){                            	//
				if (check_chunk[check_chunk.size()-1]  == '('){  	// If the strig has not been detected as corrupted
					scores[scores.size() - 1] *= 5;          	// this part counts each characters remaining in 
					scores[scores.size() - 1] += 1;          	// the storing string (that is, characters that have no
					check_chunk.erase(check_chunk.end() - 1);	// pairing) and increase the score accordingly for each
				} else if (check_chunk[check_chunk.size() - 1] == '['){ // string.
					scores[scores.size() - 1] *= 5;                 //
					scores[scores.size() - 1] += 2;                 //
					check_chunk.erase(check_chunk.end() - 1);       //
				} else if (check_chunk[check_chunk.size() - 1] == '{'){ //
					scores[scores.size() - 1] *= 5;                 //
					scores[scores.size() - 1] += 3;                 //
					check_chunk.erase(check_chunk.end() - 1);       //
				} else if (check_chunk[check_chunk.size() - 1] == '<'){ //
					scores[scores.size() - 1] *= 5;                 //
					scores[scores.size() - 1] += 4;                 //
					check_chunk.erase(check_chunk.end() - 1);       //
				}							//
			}								//
		}									//	PART 2

	}

	int max_val_index;
	int min_val_index;

	cout << "size "  << scores.size() << endl;
	while (scores.size() > 1){								// PART 2
		max_val_index = max_element(scores.begin(), scores.end()) - scores.begin();	//
		cout << "REMOVING " << scores[max_val_index] << endl;                      	// Removes min and max values from scores
		scores.erase(scores.begin()+ max_val_index);                               	// until only one value remains.
		min_val_index = min_element(scores.begin(), scores.end()) - scores.begin();	// This valu is output2.
		cout << "REMOVING " << scores[min_val_index] << endl;                      	//
		scores.erase(scores.begin()+ min_val_index);                               	//
	}											// PART 2

	output2 = scores[0];


	cout << endl << "######################################" << endl << endl;


	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl << endl;

	cout << "######################################" << endl << endl;
	return 0;

}
