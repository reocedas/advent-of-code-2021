#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;


int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	
	char test;

	int output1 = 0;
	int output2 = 0;
	
	int horiz = 0;
	int horiz2 = 0;

	int depth = 0;
	int depth2 = 0;
	int aim = 0;
	
	const char* F = "forward";
	const char* D = "down";
	const char* U = "up";
	
	string input;
	int value;
	while (finput >> input >> value){
		cout << input << " " << value << endl;

		if (input == F){
			horiz = horiz + value;
			horiz2 = horiz2 + value;
			depth2 = depth2 + aim*value;
		} else if (input == D){
			depth = depth + value;
			aim = aim + value;
		} else if (input == U){
			depth = depth - value;
			aim = aim - value;
		}


	}

	output1 = horiz*depth;
	output2 = horiz2*depth2;
	finput.close();

	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl;;
	return 0;
}
