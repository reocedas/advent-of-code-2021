#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#include <cstring>
#include <sstream>
#include <map>
#include <algorithm>

using namespace std;
vector<vector<int>> visited;

int find_basin_size(vector<int> cell, vector<vector<int>> floor){
	
	/* recursive function for PART 2
	 * places the current cell inside the "visited" cells (global variable))
	 * depending on the value of x and y (boudary-wise) check if the neighboring cells have already been visited
	 * or if it contains a 9
	 * if not, the function calls itself with the new cell coordinates we want to check
	 * if yes, passes and check the other neigbours
	 * when all the neighbours have been checked, returns the size
	 *
	 * Problem : hard coded. Maybe there is a way to to the code prettier?
	*/


	int x = cell[0];
	int y = cell[1];

	int H = floor.size(), W = floor[0].size();

	int basin_size = 1;

	int seen = 0;
	
	visited.push_back(cell);

	cout << "starting " << x << ":" << y << endl;
	for (int i = 0; i < visited.size(); i++){
		cout << visited[i][0] << ":" << visited[i][1] << " ";
	}
	cout << endl;
	
	if (x != 0 and y != 0 and x != H-1 and y != W-1){
		seen = 0;
		for (int i = 0; i < visited.size(); i++){
			if (visited[i][0] == x-1 and visited[i][1] == y){
				seen = 1;
				break;
			}
		}
		if (floor[x-1][y] != 9 and seen == 0){
			vector<int> new_cell{x-1, y};
			basin_size += find_basin_size(new_cell,floor);
		}
		seen = 0;
		for (int i = 0; i < visited.size(); i++){
			if (visited[i][0] == x+1 and visited[i][1] == y){
				seen = 1;
				break;
			}
		}
		if (floor[x+1][y] != 9 and seen == 0){
			vector<int> new_cell{x+1, y};
			basin_size += find_basin_size(new_cell,floor);
		}
		seen = 0;
		for (int i = 0; i < visited.size(); i++){
			if (visited[i][0] == x and visited[i][1] == y-1){
				seen = 1;
				break;
			}
		}
		if (floor[x][y-1] != 9 and seen == 0){
			vector<int> new_cell{x, y-1};
			basin_size += find_basin_size(new_cell,floor);
		}
		seen = 0;
		for (int i = 0; i < visited.size(); i++){
			if (visited[i][0] == x and visited[i][1] == y+1){
				seen = 1;
				break;
			}
		}
		if (floor[x][y+1] != 9 and seen == 0){
			vector<int> new_cell{x, y+1};
			basin_size += find_basin_size(new_cell,floor);
		}
	} else if (x == 0){
		if (y != 0 and y != W-1){
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x+1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x+1][y] != 9 and seen == 0){
				vector<int> new_cell{x+1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y-1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y-1] != 9 and seen == 0){
				vector<int> new_cell{x, y-1};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y+1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y+1] != 9 and seen == 0){
				vector<int> new_cell{x, y+1};
				basin_size += find_basin_size(new_cell,floor);
			}
		} else if (y == 0){
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x+1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x+1][y] != 9 and seen == 0){
				vector<int> new_cell{x+1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y+1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y+1] != 9 and seen == 0){
				vector<int> new_cell{x, y+1};
				basin_size += find_basin_size(new_cell,floor);
			}
		} else {
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x+1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x+1][y] != 9 and seen == 0){
				vector<int> new_cell{x+1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y-1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y-1] != 9 and seen == 0){
				vector<int> new_cell{x, y-1};
				basin_size += find_basin_size(new_cell,floor);
			}
		}
	} else if (x == H -1){
		if (y != 0 and y != W-1){
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x-1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x-1][y] != 9 and seen == 0){
				vector<int> new_cell{x-1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y-1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y-1] != 9 and seen == 0){
				vector<int> new_cell{x, y-1};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y+1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y+1] != 9 and seen == 0){
				vector<int> new_cell{x, y+1};
				basin_size += find_basin_size(new_cell,floor);
			}
		} else if (y == 0){
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x-1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x-1][y] != 9 and seen == 0){
				vector<int> new_cell{x-1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y+1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y+1] != 9 and seen == 0){
				vector<int> new_cell{x, y+1};
				basin_size += find_basin_size(new_cell,floor);
			}
		} else {
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x-1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x-1][y] != 9 and seen == 0){
				vector<int> new_cell{x-1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y-1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y-1] != 9 and seen == 0){
				vector<int> new_cell{x, y-1};
				basin_size += find_basin_size(new_cell,floor);
			}
		}


	} else if (y == 0){
		if (x != 0 and x != W-1){
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x-1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x-1][y] != 9 and seen == 0){
				vector<int> new_cell{x-1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x+1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x+1][y] != 9 and seen == 0){
				vector<int> new_cell{x+1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y+1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y+1] != 9 and seen == 0){
				vector<int> new_cell{x, y+1};
				basin_size += find_basin_size(new_cell,floor);
			}
		} else if (x == 0){
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x+1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x+1][y] != 9 and seen == 0){
				vector<int> new_cell{x+1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y+1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y+1] != 9 and seen == 0){
				vector<int> new_cell{x, y+1};
				basin_size += find_basin_size(new_cell,floor);
			}
		} else {
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x-1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x-1][y] != 9 and seen == 0){
				vector<int> new_cell{x-1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y+1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y+1] != 9 and seen == 0){
				vector<int> new_cell{x, y+1};
				basin_size += find_basin_size(new_cell,floor);
			}
		}
	} else if (y == W-1){
		if (x != 0 and x != W-1){
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x-1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x-1][y] != 9 and seen == 0){
				vector<int> new_cell{x-1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x+1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x+1][y] != 9 and seen == 0){
				vector<int> new_cell{x+1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y-1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y-1] != 9 and seen == 0){
				vector<int> new_cell{x, y-1};
				basin_size += find_basin_size(new_cell,floor);
			}
		} else if (x == 0){
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x+1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x+1][y] != 9 and seen == 0){
				vector<int> new_cell{x+1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y-1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y-1] != 9 and seen == 0){
				vector<int> new_cell{x, y-1};
				basin_size += find_basin_size(new_cell,floor);
			}
		} else {
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x-1 and visited[i][1] == y){
					seen = 1;
					break;
				}
			}
			if (floor[x-1][y] != 9 and seen == 0){
				vector<int> new_cell{x-1, y};
				basin_size += find_basin_size(new_cell,floor);
			}
			seen = 0;
			for (int i = 0; i < visited.size(); i++){
				if (visited[i][0] == x and visited[i][1] == y-1){
					seen = 1;
					break;
				}
			}
			if (floor[x][y-1] != 9 and seen == 0){
				vector<int> new_cell{x, y-1};
				basin_size += find_basin_size(new_cell,floor);
			}
		}
	}

	visited.push_back(cell);
	return basin_size;

}





int main()
{
	ifstream finput;

	finput.open("input.dat",ios::in);
	string sinput,mychar;

	vector<vector<int>> input;
	vector<vector<int>> minima;
	int len=0;
	
	int output1 = 0;
	int output2 = 0;			//output variables


	while (finput >> sinput){		//read the input line
		input.push_back(vector<int>());
		for (int i = 0; i < sinput.size();i++){
			mychar = sinput[i];
			cout << stoi(mychar) << " ";
			input[len].push_back(stoi(mychar));	
		}
		cout << endl;
		len ++;
	}

		len = sinput.size();


	////////////////////////////////////////////////////////////// PART 1
	//
	//
	// For all the cells, check all the neighbours
	// if they are all greater, congrats! you found a minima

	for (int i = 1; i < input.size()-1; i++){
		for (int j = 1; j < len-1; j++){

			if (input[i][j]< input[i+1][j] and
			    input[i][j]< input[i-1][j] and
			    input[i][j]< input[i][j+1] and
			    input[i][j]< input[i][j-1]){			
				cout << "FOUND " << i << " " << j << endl;
				minima.push_back(vector <int> {i,j});
				output1 += input[i][j] + 1;
				cout << output1 << endl;
			}
		}
	}

	for (int j = 1; j < len - 1; j++){
			if (input[0][j]< input[0+1][j] and
			    input[0][j]< input[0][j+1] and
			    input[0][j]< input[0][j-1]){
					cout << "FOUND " << 0 << " " <<  j << endl;
				minima.push_back(vector <int> {0,j});
				output1 += input[0][j] + 1;
				cout << output1 << endl;
			}
			if (input[input.size()-1][j]< input[input.size()-2][j] and
			    input[input.size()-1][j]< input[input.size()-1][j+1] and
			    input[input.size()-1][j]< input[input.size()-1][j-1]){
					cout << "FOUND " << input.size() -1 << " " <<  j << endl;
				minima.push_back(vector <int> {input.size()-1,j});
				output1 += (input[input.size() - 1][j] + 1);
				cout << output1 << endl;
			}
	}

	for (int i = 1; i < input.size() - 1; i++){

			if (input[i][0]< input[i+1][0] and
			    input[i][0]< input[i-1][0] and
			    input[i][0]< input[i][0+1]){
					cout << "FOUND " << i << " " <<  0 << endl;
				minima.push_back(vector <int> {i,0});
				output1 += (input[i][0] + 1);
				cout << output1 << endl;
			}


			if (input[i][len - 1]< input[i+1][len - 1 ] and
			    input[i][len - 1]< input[i-1][len - 1 ] and
			    input[i][len - 1]< input[i][len - 2]){
				cout << "FOUND " << i << " " <<  len-1 << endl;
				minima.push_back(vector <int> {i,len-1});
				output1 += (input[i][len-1] + 1);
				cout << output1 << endl;
			}
	}

	if (input[0][0] < input[1][0] and input[0][0] < input[0][1]){
		cout << "FOUND " << 0 << " " <<  0 << endl;
		minima.push_back(vector <int> {0,0});
		output1 += input[0][0] + 1;
	}
	if (input[input.size() - 1][len -1] < input[input.size() - 2 ][len - 1] and input[input.size() - 1][len - 1] < input[input.size() - 1][len - 2]){
		cout << "FOUND " << input.size() - 1 << " " <<  len-1 << endl;
		minima.push_back(vector <int> {input.size() - 1, len - 1});
		output1 += input[input.size() - 1][len -1] + 1;
	}
	if (input[input.size() - 1][0] < input[input.size() - 2 ][0] and input[input.size() - 1][0] < input[input.size() - 1][1]){
		cout << "FOUND " << input.size()-1 << " " <<  0 << endl;
		minima.push_back(vector <int> {input.size() - 1, 0});
		output1 += input[input.size() - 1][0] + 1;
	}
	if (input[0][len -1] < input[1][len - 1] and input[0][len - 1] < input[0][len - 2]){
		cout << "FOUND " << 0 << " " <<  len-1 << endl;
		minima.push_back(vector <int> {0, len - 1});
		output1 += input[0][len -1] + 1;
	}
		
	
	////////////////////////////////////////////////////////////// PART 1

	////////////////////////////////////////////////////////////// PART 2
	vector<int> basins_sizes,cell;
	
	for (int i = 0; i < minima.size(); i++){
		visited.clear();
		cell = minima[i];
		
		basins_sizes.push_back(find_basin_size(cell, input));

		//cin.get();
	}
	

	// Extract the 3 larger values and multiplie them together produce the right output 2

	int max_val_index = max_element(basins_sizes.begin(),basins_sizes.end()) - basins_sizes.begin();
	output2 += basins_sizes[max_val_index];
	basins_sizes.erase(basins_sizes.begin()+max_val_index);
	max_val_index = max_element(basins_sizes.begin(),basins_sizes.end()) - basins_sizes.begin();
	output2 *= basins_sizes[max_val_index];
	basins_sizes.erase(basins_sizes.begin()+max_val_index);
	max_val_index = max_element(basins_sizes.begin(),basins_sizes.end()) - basins_sizes.begin();
	output2 *= basins_sizes[max_val_index];
	
	////////////////////////////////////////////////////////////// PART 2



	cout << endl << "######################################" << endl << endl;


	finput.close();
	cout << "Output 1 = " << output1 << endl;
	cout << "Output 2 = " << output2 << endl << endl;

	cout << "######################################" << endl << endl;
	return 0;

}
