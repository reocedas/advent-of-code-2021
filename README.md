# Advent of code 2021



## Getting started

Source code for Advent of code 2021 challenge. This year, written in C++.

## Structure

For each day, one folder. In the folder, the testing data provided on the AOC website are in the file test.dat, the actual data are in input.dat, and the code to solve the problem is in #.cpp (# being the number of the challenge). the file # without extention is the compiled binary.
